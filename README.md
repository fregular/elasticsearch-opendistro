# Elasticsearch opendistro 

Ejemplo de configuración de un cluster de elasticsearch instalado por en helm

Aca hay un ejemplo de ingress, service, pv y pvc

instale Elastic open-distro

[Instalación opendistro helm](https://opendistro.github.io/for-elasticsearch-docs/docs/install/helm/)

Luego conecte el pvc al pv que necesite. Luego edite los service e ingress que estan de ejemplo.

Recuerde cambiar los nombre de los service en el ingress.

El acceso al kibana y Elastic por medio de ingress es por el puerto 80, si necesita agrega una capa de seguridad
debe configurarlo.
